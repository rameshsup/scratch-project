<center>
    <img src="https://gitlab.com/ibm/skills-network/courses/placeholder101/-/raw/master/labs/module%201/images/IDSNlogo.png" width="300" alt="cognitiveclass.ai logo"  />
</center>



# Creating a Python Package



Estimated time needed: **30** minutes



## Objectives

In this lab you will :

-   Create a module named basic
-   Add two functions to the module basic
-   Create a module named statistics
-   Add two functions to the module statistics
-   Create a python package named mymath
-   Verify that the package is working


## Lab


### Create Package

* Let us create a folder named mymath.
* Create module basic
    * Create a file named basic.py
    * Copy two functions square and double in the files we created in the Unit Testing lab.
* Create module statistics
    * Create a file named statistics.py
    * Write two functions mean and median in the files we created in the Unit Testing lab
* Create the file `__init__.py`
Add the below two lines of code into the `__init__.py`

```
from . import basic
from . import statistics
```


Now your directory structure should look like

```python
mymath
mymath/__init__.py
mymath/basic.py
mymath/statistics.py
```

## Verify the package


* Open a bash terminal.
* cd to the folder where the mymath package exists.
* Open python interpreter by running the command `python` on the shell
* At the python prompt type `import mymath`
* If the above command runs without errors, it is in indication that the mymath package is successfully loaded.
* At the python prompt type `mymath.basic.add(3,4)`
* You should see an output `7` on the screen.


## Authors



Ramesh Sannareddy



### Other Contributors



Rav Ahuja


## Change Log



| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                 |
| ----------------- | ------- | ----------------- | ---------------------------------- |
| 2020-11-25        | 0.1     | Ramesh Sannareddy | Created initial version of the lab |



 Copyright © 2020 IBM Corporation. This notebook and its source code are released under the terms of the [MIT License](https://cognitiveclass.ai/mit-license?cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ).

