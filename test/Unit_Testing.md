# Hands-on Lab: Unit Testing

<center>
    <img src="https://gitlab.com/ibm/skills-network/courses/placeholder101/-/raw/master/labs/module%201/images/IDSNlogo.png" width="300" alt="cognitiveclass.ai logo"  />
</center>



# Unit Testing Lab



Estimated time needed: **30** minutes



## Objectives

After completing this lab you will be able to:

-   Write unit tests to test a function.
-   Run unit tests and interpret the results.



## Create a new file using the IDE and name it as `mymodule.py`

Copy and paste the below code into mymodule.py

```
def square(number):
    """
    This function returns the square of a given number
    """
    return number ** 2


def double(number):
    """
    This function returns twice the value of a given number
    """
    return number * 2
```
{:codeblock}

You should see a screen like this now.

![](image1.png)

## Write the unit tests for square function


Let us write test cases for these three scenarios.
* When 2 is given as input the output must be 4.
* When 3.0 is given as input the output must be 9.0.
* When -3 is given as input the output must not be -9.



## Write the unit tests for double function


Let us write test cases for these three scenarios.
* When 2 is given as input the output must be 4.
* When -3.1 is given as input the output must be -6.2.
* When 0 is given as input the output must be 0.

## Create a new file using the IDE and name it as `testcase.py`

Copy and paste the below code into testcase.py

```
import unittest

from mymodule import square, double

class TestSquare(unittest.TestCase): 
    def test1(self): 
        self.assertEqual(square(2), 4) # test when 2 is given as input the output is 4.
        self.assertEqual(square(3.0), 9.0)  # test when 3.0 is given as input the output is 9.0.
        self.assertNotEqual(square(-3), -9)  # test when -3 is given as input the output is not -9.
        

class TestDouble(unittest.TestCase): 
    def test1(self): 
        self.assertEqual(double(2), 4) # test when 2 is given as input the output is 4.
        self.assertEqual(double(-3.1), -6.2) # test when -3.1 is given as input the output is -6.2.
        self.assertEqual(double(0), 0) # test when 0 is given as input the output is 0.
        
unittest.main()

```
{:codeblock}

You should see a screen like this now.

![](image2.png)


## Run tests
To run tests, select the `testcase.py` file in the IDE and click on the 'Play' button.

This will run the tests.

You should see a screen like this now.

![](image3.png)


## Authors



Ramesh Sannareddy



### Other Contributors



Rav Ahuja


## Change Log



| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                 |
| ----------------- | ------- | ----------------- | ---------------------------------- |
| 2020-11-25        | 0.1     | Ramesh Sannareddy | Created initial version of the lab |



 Copyright © 2020 IBM Corporation. This notebook and its source code are released under the terms of the [MIT License](https://cognitiveclass.ai/mit-license?cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ&cm_mmc=Email_Newsletter-_-Developer_Ed%2BTech-_-WW_WW-_-SkillsNetwork-Courses-IBM-DA0321EN-SkillsNetwork-21426264&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ).

